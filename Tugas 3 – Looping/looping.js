// Tugas No 1 While
console.log("\n==========SOAL NO 1=============");

var i = 1;
var j = 20;

while(i<=20){
    if(i==1)
        console.log("LOOPING PERTAMA");
    if(i%2 == 0)
        console.log(i + " - I love coding");
    if(i==20){
        console.log("LOOPING KEDUA");
        while(j>1){
            if(j%2 == 0)
            console.log(j + " - I will become a mobile developer");
        j--;
        }
        
    }
    i++;
    
}


//Tugas No 2 FOR
var i = 1;
console.log("\n==========SOAL NO 2=============");
for (var i=1;i<=20;i++){
    if(i%2==1 && i%3 != 0)
        console.log(i + " - Santai");
    if(i%2==0)
        console.log(i + " - Berkualitas");
    if(i%3==0 && i%2==1) 
        console.log(i + " - I Love Coding");
}

console.log("\n==========SOAL NO 3=============");

// Dear Trainer, Mohon ijin 
// Untuk soal 3 dan soal 4 saya menjawab 2 versi 
// dimana 1 jawaban ada yg sytaxnya di luar materi

console.log("\n==========SOAL NO 3 MENGGUNAKAN CONSOLE.LOG=============");
var i=1;
var hashtag = "#"
var j=1;

for(var i=1;i<=4;i++){
    for(var j=1;j<8;j++){
        hashtag=hashtag + "#";
    }
    console.log(hashtag);
    hashtag = "#";
}


// MENGGUNAKAN STDOUT
console.log("\n==========SOAL NO 3 MENGGUNAKAN STDOUT=============");
var i=1;
while(i<=32){
    process.stdout.write("#");  
    if(i==8 || i==16 || i==24)
        process.stdout.write("\n");
    i++;
}

//soal no 4
console.log("\n==========SOAL NO 4 MENGGUNAKAN STDOUT=============");
var i = 1;
for(var i=1;i<=7;i++){
    for(j=1;j<=i;j++){
        process.stdout.write("#");    
    }
    process.stdout.write("\n");
}

console.log("\n==========SOAL NO 4 MENGGUNAKAN CONSOLE LOG=============");
var i = 1;
var hashtag = "#"
for(var i=1;i<=7;i++){
    console.log(hashtag);
    hashtag = hashtag + "#";
}

// OUTPUT SAMA
// #
// ##
// ###
// ####
// #####
// ######
// #######

// SOAL NO 5
console.log("\n==========SOAL NO 5=============");
var i = 1;
for(var i=1;i<=8;i++){
    if(i%2==1) //BARIS GANJIL
        console.log(" # # # #");
    else 
        console.log("# # # # ");
}
console.log("\n==========SOAL NO 5 tes=============");
var i=1;
var hashtag = "#";

for(var i=1;i<=4;i++){
    hashtag = " " + hashtag;
    
}
console.log(hashtag);

//OUTPUT SOAL 5
//  # # # #
// # # # #
//  # # # #
// # # # #
//  # # # #
// # # # #
//  # # # #
// # # # #