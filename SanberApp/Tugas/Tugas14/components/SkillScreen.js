import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    Text
} from 'react-native'

import SkillItems from './SkillItems';
import skillData from './skillData';
import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class App extends Component {

    state = {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }

    render() {

        return (
            <View style={styles.container}>

                <View style={styles.body}>
                    <View style={styles.logo}>
                        <Image source={require('./images/logo.png')} style={{height:50, width:220}} />
                    </View>
                    
                    <View style={styles.portfolio}>
                        <MaterialCommunityIcons name="account-tie" size={50} color="#B4E9FF" />
                        <View style={{flexDirection: 'column'}}>
                            <Text>
                                Selamat Datang,
                            </Text>
                            <Text>
                                Ikra Dewantara
                            </Text> 
                        </View>  
                    </View>  


                    <View style={{alignItems: 'stretch'}}>
                        <Text style={[styles.textTitle,styles.label]}>
                            SKILL
                        </Text>
                    </View>

                    <View style={{ borderBottomColor: '#B4E9FF', borderBottomWidth: 4, }}/>

                    <View style={{flexDirection: 'row'}}>
                        <View style={styles.box}>
                            <Text>
                                Library/Framework
                            </Text>
                        </View>
                        <View style={styles.box}>
                            <Text>
                                Bahasa Pemrograman
                            </Text>
                        </View>
                        <View style={styles.box}>
                            <Text>
                                Teknologi
                            </Text>
                        </View>
                    </View>

                </View>
            
                <FlatList 
                    data={skillData.items}
                    renderItem={(skill)=><SkillItems skill={skill.item}/>}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:10}} />}
                />

            
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo:{
        // position: absolute,
        width: 187,
        height: 51,
        left: 187,
        top: 0,
    },
    layout: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.05)',
    },
    box: {
        padding: 10,
        backgroundColor: '#B4E9FF',
        margin: 5,
        borderColor : 'yellow',
        borderRadius: 10,
    },
    text: {
        fontSize: 36,
        color: '#003366',
        fontWeight: 'bold'
    },
    textDesc: {
        fontSize: 18,
        color: '#003366',
        fontWeight: 'normal'
    },
    textTitle: {
        fontSize: 36,
        color: '#003366',
        fontWeight: 'bold'
    },
    textSubTitle: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold',
    },
    textName: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    textProfile: {
        fontSize: 18,
        color: '#3EC6FF',
        fontWeight: 'bold',
        fontStyle: "italic"
    },
    boxPortfolio: {
        padding: 10,
        fontSize: 18,
        color: '#3EC6FF',
        fontWeight: 'bold',
        backgroundColor: '#EFEFEF',
    },
    boxSubTitle: {
        padding: 5,
        borderStyle  : "solid",
        fontSize: 18,
        borderColor : "black",
        color: '#fff',
        fontWeight: 'bold',
        backgroundColor: '#F0DEDE',
    },
    portfolio: {
        flexDirection: 'row',
        margin: 5,
        alignItems:'center'
    },
    profile: {
        flexDirection: 'column',
        margin: 5,
        alignItems:'center'
    },
    label: {
        padding: 4,
        // color: '#003366'
    },
    body: {
        paddingTop: 50,
    }
})
