/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor(subject, points, email) {
    this._subject = subject;
    this._points = points;
    this._email = email;
}

average(){
    let sum = 0;
    for (let i = 0; i < this._points.length; i++) {
        sum = sum + this._points[i];
    }
    let result = sum/this._points.length;
    return result;
}
}

//TEST CASE
let dataSoal1 = [0,1,2,3,4,5,6];
let myScore = new Score("average",dataSoal1)
console.log(myScore.average());

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi personObject yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  // code kamu di sini
  let header = ["email", "subject", "points"];
  let output = []
  for (let i = 0; i < data.length; i++) {
    let personObj = {}  // reset dikosongin lg

    //inisialisasi header berisi (email, subject)
    personObj[header[0]] = data[i][0]
    personObj[header[1]] = subject

    // dikategoriin berdasarkan subject. ditambahkan points
    for (let j = 1; j < data[i].length; j++){
      switch(subject) {
        case "quiz-1":
          personObj[header[2]] = data[i][1]
          break;
        case "quiz-2":
          personObj[header[2]] = data[i][2]
          break;
        case "quiz-3":
          personObj[header[2]] = data[i][3]
          break;
      }
    }
    output[i] = personObj
  }
    output.shift(); //menghapus array pertama
    console.log(output)
  
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  // code kamu di sini
  let sum = (value, currVal) => value + currVal;
  
  for (let i = 1; i < data.length; i++) {
    let nilai = [];
    for (let index = 1; index < data[i].length; index++) {
      nilai.push(data[i][index]);
    }
  
    let average = nilai.reduce(sum) / nilai.length;
    let predikat = (average > 90) ? "honour" 
                 : (average > 80) ? "graduate" 
                 : (average > 70) ? "participant" 
                 : "";
    
    console.log(
      i + "." + " Email: " + data[i][0] + "\n" + 
      "Rata-rata: " + average.toFixed(1) + "\n" + 
      "Predikat: " + predikat + "\n"

    );


 
    
  }

  
}

recapScores(data);
