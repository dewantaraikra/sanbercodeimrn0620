console.log("======== SOAL 1 ========")

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)
var arr = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people = {}
var checkYear = "";

function arrayToObject(arr) {
    // Code di sini 
    
    for(var i=0;i<arr.length;i++){
        if(arr[i][3] == undefined || arr[i][3] > thisYear){
            checkYear = "Invalid birth year"
        }else{
            checkYear = thisYear - arr[i][3]
        }
        var people = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: checkYear
        }
        
        console.log(i+1 + ". " + people.firstName.concat(" " + people.lastName + ":"),people)
    }
}

arrayToObject(arr) 




console.log("======== SOAL 2 ========")
 
var transaction = {}
var sisa = 0
var listPurchased = []

function shoppingTime(memberId, money){
    if(memberId == undefined || (memberId == '' && money != '')){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if(money<50000){
        return "Mohon maaf, uang tidak cukup"
    }
    var sisaUang = money;
    if(sisaUang>=1500000){
        listPurchased.push("Sepatu Stacattu")
        sisaUang = sisaUang - 1500000
    }if(sisaUang>=500000){
        listPurchased.push("Baju Zoro")
        sisaUang = sisaUang - 500000
    }if(sisaUang>=250000){
        listPurchased.push("Baju H&N")
        sisaUang = sisaUang - 250000
    }if(sisaUang>=175000){
        listPurchased.push("Sweater Uniklooh")
        sisaUang = sisaUang - 175000
    }if(sisaUang>=50000){
        listPurchased.push("Casing Handphone")
        sisaUang = sisaUang - 50000
    }
    transaction = {
        memberId : memberId,
        money : money,
        listPurchased: listPurchased,
        changeMoney : sisaUang

    }
    listPurchased = []
    sisaUang = 0
    return transaction

}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
 //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }

console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja




console.log("======== SOAL 3 ========")

var price = 2000;
var listpenumpang = {}
var arrPenumpang = [];
var trip;
var tripawal;
var triptujuan;
var output = [];
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    if(arrPenumpang!=''){
        for(var i=0;i<arrPenumpang.length;i++){
            tripawal = arrPenumpang[i][1].charCodeAt(0);
            triptujuan = arrPenumpang[i][2].charCodeAt(0); 
            trip = triptujuan - tripawal
            var listpenumpang = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                tujuan: arrPenumpang[i][2],
                bayar: trip * 2000
            }
            output.push(listpenumpang);
        
        }
        
        return output    
    } else{
        output = [];
        return output
    }
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]