console.log("====== SOAL NO 1 =======")
//Tulis code function di sini
function teriak(){
    return "Halo Sanbers!"; 
}
//END CODE

console.log(teriak())

console.log("====== SOAL NO 2 =======")

//Tulis code function di sini
function kalikan(num1, num2){
    return num1 * num2;
}
//END CODE

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

console.log("====== SOAL NO 3 =======")

//Tulis kode function di sini
function introduce(name, age, address,  hobby){
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
}
//END CODE
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 