console.log("===== SOAL NO 1 =====")
var output=[];

function range(startNum, finishNum){
   
    if(startNum>finishNum){
        for(var i=startNum;i>=finishNum;i--){
            output.push(i)
        }
    }else if(startNum<finishNum){
        for(var i=startNum;i<=finishNum;i++){
            output.push(i)
        } 
    }else 
        output= -1
    
    return output
} 

console.log(range(54,50));


console.log("===== SOAL NO 2 =====")

var output = [];

function rangeWithStep(startNum, finishNum, step) {
    var temp = startNum;
    
    if(startNum>finishNum){
        do{
            output[0]=startNum;
            temp = temp - step;
            output.push(temp);
        }while(temp>=finishNum)
        
        //Check last element < finishNum
        var ArrayLength = output.length -1;
        if(output[ArrayLength]<finishNum)
            output.pop();

    }else if(startNum<finishNum){
        do{
            output[0]=startNum;
            temp = temp + step;
            output.push(temp);
        }while(temp<=finishNum)
        
        //Check last element < finishNum
        var ArrayLength = output.length -1;
        if(output[ArrayLength]>finishNum)
            output.pop();
    }

    return output
}

console.log(rangeWithStep(11,23,3));

console.log("===== SOAL NO 3 =====")

var output = [];

function sum(startNum, finishNum, step) {
    var temp = startNum;
    if(step==undefined)
        step = 1;
    if(finishNum==undefined)
        finishNum = 1;
    
    if(startNum>=finishNum){
        do{
            output[0]=startNum;
            temp = temp - step;
            output.push(temp);
        }while(temp>=finishNum)
        
        //Check last element < finishNum
        var ArrayLength = output.length -1;
        if(output[ArrayLength]<finishNum)
            output.pop();
        
        // fungsi sum nya
        var sum = 0;
        for(var i=0;i<ArrayLength;i++){
            sum = sum + output[i];
        }

    }else if(startNum<finishNum){
        do{
            output[0]=startNum;
            temp = temp + step;
            output.push(temp);
        }while(temp<=finishNum)
        
        //Check last element < finishNum
        var ArrayLength = output.length -1;
        if(output[ArrayLength]>finishNum)
            output.pop();
        
        // fungsi sum nya
        var sum = 0;
        for(var i=0;i<ArrayLength;i++){
            sum = sum + output[i];
        }
    }
    else
        sum = 0;

    return sum
}

// console.log(sum(1,10)); // 55
// console.log(sum(5, 50, 2)); // 621
// console.log(sum(15,10)); // 75
console.log(sum(20,10,2)); // 90
// console.log(sum(1)) // 1
// console.log(sum()) // 0 

console.log("===== SOAL NO 4 =====")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

 //array [y,x]
 var panjangY=input.length      // 4
 var panjangX=input[0].length   // 5 

function dataHandling (input){

    for(var i=0;i<panjangY;i++){
        console.log("Nomor ID:  " + input[i][0])
        console.log("Nama Lengkap:  " + input[i][1])
        console.log("TTL:  " + input[i][2] + " " + input[i][3])
        console.log("Hobi:  " + input[i][4] + "\n")
    }
   
}

dataHandling(input);

console.log("===== SOAL NO 5 =====")


function balikKata(kata){
    // var temp="";
    var output="";
    panjang=kata.length-1;
    for(var i=panjang;i>=0;i--)
        output = output + kata[i];
    
    return output;
}

console.log(balikKata("I am Sanbers")); // srebnaS ma I 

console.log("===== SOAL NO 6 =====")

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
var output1 = [];
var output2;
var output3 = [];

var sortdate = [];

function dataHandling2(input){
    //keluaran baris 1
    input.splice(1,1,"Roman Alamsyah Elsharawy");
    input.splice(2,1,"Provinsi Bandar Lampung");
    input.splice(4,1,"Pria");
    input.splice(5,0,"SMA Internasional Metro");
    output1 = input.slice();
    console.log(output1);   //Outputnya ARRAY

    //keluaran baris 2
    var bulan = input[3].split("/");
    bulan = bulan[1];
    bulan = parseInt(bulan);

    switch(bulan) {
        case 01:   
            bulan = "Januari"; 
            break;
        case 02:   
            bulan = "Februari"; 
            break;
        case 03:   
            bulan = "Maret"; 
            break;
        case 04:   
            bulan = "April"; 
            break;
        case 05:   
            bulan = "Mei"; 
            break;
        case 06:   
            bulan = "Juni"; 
            break;
        case 07:   
            bulan = "Juli"; 
            break;
        case 08:   
            bulan = "Agustus"; 
            break;
        case 09:   
            bulan = "September"; 
            break;
        case 10:   
            bulan = "Oktober"; 
            break;
        case 11:   
            bulan = "November"; 
            break;
        case 12:   
            bulan = "Desember"; 
            break;
        default:  console.log('Masukkan bulan antara 1 s/d 12');
    }

    output2 = bulan;
    console.log(output2);   // OUTPUTNYA MEI

    //keluaran baris 3
    var ambiltanggal = input[3].split("/");
    sortdate[0]=ambiltanggal[2];
    sortdate[1]=ambiltanggal[0];
    sortdate[2]=ambiltanggal[1]; 
    output3 = sortdate.slice();

    console.log(output3);  //buat sort descending (Output:["1989", "21", "05"])
    
    //keluaran baris 4
    ambiltanggal.slice(input[3].split("/")); //[ '21', '05', '1989' ]
    var output4 = ambiltanggal.join("-")
    console.log(output4);   // OUTPUTNYA TANGGAL 21-05-1989

    //keluaran baris 5
    var ambilnama = input[1];
    var output5 = ambilnama.slice(0,14);    //cuma ambil 15 karakter
    console.log(output5); //
}

dataHandling2(input);

//OUTPUT KESELURUHAN

/*

[
  '0001',
  'Roman Alamsyah Elsharawy',
  'Provinsi Bandar Lampung',
  '21/05/1989',
  'Pria',
  'SMA Internasional Metro'
]
Mei
[ '1989', '21', '05' ]
21-05-1989
Roman Alamsyah

*/