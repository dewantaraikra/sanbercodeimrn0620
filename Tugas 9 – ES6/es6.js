console.log("##### SOAL 1 #####")

const golden = goldenFunction = () =>{
    return console.log("this is golden!!")
}

golden()

console.log("##### SOAL NO 2 #####")

const newFunction = literal = (firstName, lastName) => {
  return {
      fullName: () => console.log(`${firstName} ${lastName}`)
  }
}

// Driver Code
newFunction("William", "Imoh").fullName()

console.log("##### SOAL NO 3 #####")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject

// Driver Code
console.log(firstName, lastName, destination, occupation)

console.log("##### SOAL NO 4 #####")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

// Driver Code
console.log(combined)

console.log("##### SOAL NO 5 #####")

const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before) 
