import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import API from './api';
// import data from './data.json';
import { useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import { FlatList } from 'react-native-gesture-handler';


const vw = Dimensions.get('window').width;
const vh = Dimensions.get('window').height;



const NewTicketList = (props) => {
	
	const id = props.data.idBulletin

	return (
		<TouchableOpacity onPress={() => props.openDetails(id)}>
			<View style={styles.ticketContainer}>
        <Text style={styles.ticketTitle}>{props.data.title}</Text>
			</View>
		</TouchableOpacity>
	);
};




const HomeScreen = ({ navigation }) => {
	//REDUX
	const isLoading = useSelector(state => state.isLoading);
	const dispatch = useDispatch();

	const [data, setData] = useState(null);
	dispatch(loading);

	useEffect(() => {
		const fetchData = async () => {
			setData(await API.discover());
			dispatch(loaded);
		};

		fetchData();
	}, []);



	const openDetails = (id) => {
		navigation.navigate('Details', {id: id})
	}

	if (isLoading) return <Text>Loading....</Text>

	return (
		<ScrollView>
			<View style={styles.container}>
				<SearchBar placeholder='Search Ticket' />
				<View style={styles.contentWrapper}>
					<Text style={styles.sectionHeader}>List Ticket</Text>
					<FlatList
						data={data}
						renderItem={(ticket) => <NewTicketList data={ticket.item} openDetails={openDetails}/>}
						keyExtractor={(item) => item.id.toString()}
						numColumns={1}
					/>
				</View>
			</View>
		</ScrollView>
	);
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: vw,
    alignItems: 'center',
    paddingHorizontal: 0.05 * vw,
  },
  contentWrapper: {
    flex: 1,
    marginVertical: 0.07 * vw,
    width: '100%',
  },
  sectionHeader: {
    fontSize: 24,
    fontWeight: '600',
    textAlign: 'left',
    marginBottom: 0.07 * vw,
    color: '#F68D29'
  },

  ticketContainer: {
    flexDirection: 'row',
    width: 0.9 * vw,
    height: 0.9 * vw * 0.5,
    borderRadius: 10,
    overflow: 'hidden',
    elevation: 3,
    marginBottom: 0.03 * vh,
    backgroundColor: 'white',
  },
  // ticketMedia: {
  //   flex: 1,
  //   height: '100%',
  // },
  // ticketContent: {
  //   flex: 1.5,
  //   paddingHorizontal: 0.05 * vw,
  //   paddingVertical: 0.05 * vw,
  // },
  ticketTitle: {
    fontSize: 20,
    marginBottom: 0.02 * vw,
    fontWeight: '600',
  },

});



export default HomeScreen;