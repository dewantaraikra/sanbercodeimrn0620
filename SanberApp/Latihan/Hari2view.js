import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, ScrollView, FlatList } from 'react-native'


const rows = [
  { id: 0, text: 'View' },
  { id: 1, text: 'Text' },
  { id: 2, text: 'Image' },
  { id: 3, text: 'ScrollView' },
  { id: 4, text: 'ListView' },
]

const extractKey = ({ id }) => id

export default class App extends Component {
  // render() {
    // return (
      // <View style={styles.container}>
      //   <View style={styles.box} />
      //   <Text style={styles.text}>Hello!</Text>
      //   <Image 
      //       style={styles.image}
      //       source = {require("./ipb.png")}
      //   />
      // </View>

      // <ScrollView style={styles.container}>
      //   <View style={styles.boxLarge} />
      //   <ScrollView horizontal>
      //     <View style={styles.boxSmall} />
      //     <View style={styles.boxSmall} />
      //     <View style={styles.boxSmall} />
      //   </ScrollView>
      //   <View style={styles.boxLarge} />
      //   <View style={styles.boxSmall} />
      //   <View style={styles.boxLarge} />
      // </ScrollView>

      renderItem = ({ item }) => {
        return (
          <Text style={styles.row}>
            {item.text}
          </Text>
        )
      }

      render() {
        return (
          <FlatList
            style={styles.container}
            data={rows}
            renderItem={this.renderItem}
            keyExtractor={extractKey}
          />
        );
      }
    }
      


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: 'skyblue',
    borderWidth: 2,
    borderColor: 'steelblue',
    borderRadius: 20,
  },
  text: {
    backgroundColor: 'whitesmoke',
    color: '#4A90E2',
    fontSize: 24,
    padding: 10,
  },
  boxSmall: {
    width: 200,
    height: 200,
    marginBottom: 10,
    marginRight: 10,
    backgroundColor: 'skyblue',
  },
  boxLarge: {
    width: 300,
    height: 300,
    marginBottom: 10,
    marginRight: 10,
    backgroundColor: 'steelblue',
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
  },
})