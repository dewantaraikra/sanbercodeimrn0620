const BASE_URL = 'http://sifora.irshatech.com/api/bulletin_getList';

class API {
	static async discover() {
		try {
			const url = `${BASE_URL}`;
			const response = await fetch(url);
			const { results } = await response.json();
			return results;
		} catch (error) {
			return error;
		}
	}

	static async getID(id) {
		try {
			const url = `${BASE_URL}`;
			const response = await fetch(url);
			const results = await response.json();
			return results;
		} catch (error) {
			return error;
		}
	}
}

export default API;
